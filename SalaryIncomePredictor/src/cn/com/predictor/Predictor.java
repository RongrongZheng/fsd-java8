package cn.com.predictor;

import java.math.BigDecimal;
import java.util.Scanner;

import dnl.utils.text.table.TextTable;

public class Predictor {
	
	public static void main(String[] args) {
		
		double  saraly = inputParamsforDouble("起薪: ",1);
		double  incrementPrent = inputParamsforDouble("增量百分比(值得范围0-1): ",2);
		int  frequencyOfReceivingIncrements = inputParamsforInt("收到增量的频率(eg: 2):",3);
		double  deductionPrent = inputParamsforDouble("扣除百分比(值得范围0-1):",2);	
		int  frequencyOfDeduction = inputParamsforInt("扣除的频率(eg: 2): ",3);
		int year = inputParamsforInt("预测(年)(eg:5): ",3);		
		String incrementReportDatas[][] = printIncrementReport(saraly, incrementPrent, frequencyOfReceivingIncrements, year); 		
		String dedutionReportDatas[][]= printDedutinReport(saraly, incrementPrent, deductionPrent, frequencyOfDeduction, year); 		
		printPredictionReport(saraly, year, incrementReportDatas,dedutionReportDatas); 
		
	}
	@SuppressWarnings("resource")
	private static int inputParamsforInt(String paramTitle, int type) {
		System.out.println(paramTitle);
		Scanner scanner = new Scanner(System.in);
		int  param = scanner.nextInt(); 	
		
		if(type == 3 && param < 1) {
			System.out.println("Error， 输入值不能小于1");
			inputParamsforInt(paramTitle,type);
		}
		return param;
		
	}
	@SuppressWarnings("resource")
	private static double inputParamsforDouble(String paramTitle,int type) {
		System.out.println(paramTitle);
		Scanner scanner = new Scanner(System.in);
		double  param = scanner.nextDouble(); 		
		if(type == 1 && param < 1) {
			System.out.println("Error， 起薪不能小于1");
			inputParamsforDouble(paramTitle,type);
		}
		if(type == 2 &&( param>1 || param<0) ){
			System.out.println("Error，输入值必须是0-1范围之间");
			inputParamsforDouble(paramTitle,type);
		}
		return param;
		
	}

	private static void printPredictionReport(double saraly, int year, String[][] incrementReportDatas, String[][] dedutionReportDatas) {	
		
		String predictionTitles[]= {"年","起薪","增量金额","扣除金额","工资增长"};
		String predictionDatas[][]= new String[year][5];
		double predictionSaraly=saraly;
		for(int i=0;i<year;i++) {
			predictionDatas[i][0]=String.valueOf(i+1);
			predictionDatas[i][1]=String.valueOf(new BigDecimal(predictionSaraly).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
			predictionDatas[i][2]=incrementReportDatas[i][4];
			predictionDatas[i][3]=dedutionReportDatas[i][4];
			double incrementMany=Double.valueOf(predictionDatas[i][2])- Double.valueOf(predictionDatas[i][3]);
			predictionDatas[i][4]=String.valueOf(new BigDecimal(incrementMany).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
			
			predictionSaraly = predictionSaraly + incrementMany ;
		}
		
		System.out.println("预测：");
		TextTable predictionTable = new TextTable(predictionTitles, predictionDatas);  
		predictionTable.printTable();
	}

	private static String[][] printDedutinReport(double saraly, double incrementPrent, double deductionPrent,
			int frequencyOfDeduction, int year) {
		String deductionReportTitles[]= {"年","起薪","扣除次数","扣除%","扣除金额"};
		String dedutionReportDatas[][]= new String[year][5];
		double dedutionSaraly=saraly;
		for(int i=0;i<year;i++) {
			dedutionReportDatas[i][0]=String.valueOf(i+1);
			dedutionReportDatas[i][1]=String.valueOf(new BigDecimal(dedutionSaraly).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
			dedutionReportDatas[i][2]=String.valueOf(frequencyOfDeduction);
			dedutionReportDatas[i][3]=String.valueOf(deductionPrent);
			double dedutionMany=dedutionSaraly-(dedutionSaraly * Math.pow((1-deductionPrent),frequencyOfDeduction));
			dedutionReportDatas[i][4]=String.valueOf(new BigDecimal(dedutionMany).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
			
			dedutionSaraly = dedutionSaraly * Math.pow((1-incrementPrent),frequencyOfDeduction);
		}
		System.out.println("扣除报告：");
		TextTable dedutionReportTable = new TextTable(deductionReportTitles, dedutionReportDatas);  
		dedutionReportTable.printTable();
		
		return dedutionReportDatas;
	}

	private static String[][] printIncrementReport(double saraly, double incrementPrent, int frequencyOfReceivingIncrements,
			int year) {
		String incrementReportTitles[]= {"年","起薪","增量数","增量%","增量金额"};
		String incrementReportDatas[][]= new String[year][5];
		double incrementSaraly=saraly;
		for(int i=0;i<year;i++) {
			incrementReportDatas[i][0]=String.valueOf(i+1);
			incrementReportDatas[i][1]=String.valueOf(new BigDecimal(incrementSaraly).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
			incrementReportDatas[i][2]=String.valueOf(frequencyOfReceivingIncrements);
			incrementReportDatas[i][3]=String.valueOf(incrementPrent);
			double incrementMany= incrementSaraly * Math.pow((1+incrementPrent),frequencyOfReceivingIncrements)-incrementSaraly;
			incrementReportDatas[i][4]=String.valueOf(new BigDecimal(incrementMany).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
			incrementSaraly = incrementSaraly * Math.pow((1+incrementPrent),frequencyOfReceivingIncrements);
			
		}
		System.out.println("增量报告：");
		TextTable incrementReportTable = new TextTable(incrementReportTitles, incrementReportDatas);  
		incrementReportTable.printTable();
		
		return incrementReportDatas;
	}

}
